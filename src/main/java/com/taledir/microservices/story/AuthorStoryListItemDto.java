package com.taledir.microservices.story;

import java.util.Date;

public class AuthorStoryListItemDto {

	private String title;

	private String route;

  private String snippet;

	private Date publicationDate;

	private Date lastModifiedDate;

	public AuthorStoryListItemDto(String title, String route, String snippet,
    Date publicationDate, Date lastModifiedDate) {
		this.title = title;
    this.route = route;
    this.snippet = snippet;
    this.publicationDate = publicationDate;
    this.lastModifiedDate = lastModifiedDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
