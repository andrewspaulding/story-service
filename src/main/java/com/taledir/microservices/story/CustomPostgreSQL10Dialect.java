package com.taledir.microservices.story;

import org.hibernate.dialect.PostgreSQL10Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;
import com.vladmihalcea.hibernate.type.array.StringArrayType;

public class CustomPostgreSQL10Dialect extends PostgreSQL10Dialect {

  public CustomPostgreSQL10Dialect() {
    super();
    this.registerFunction("string_agg", new SQLFunctionTemplate(StandardBasicTypes.STRING, "STRING_AGG(?1 || '', ?2)"));
    this.registerFunction("array_agg", new SQLFunctionTemplate(StringArrayType.INSTANCE, "ARRAY_AGG(?1 || '')"));
  }
  
}