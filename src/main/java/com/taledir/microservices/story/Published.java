package com.taledir.microservices.story;

import java.util.Date;

public class Published {
  
  private Long id;
  
  String title;

  private String route;

  private Date publicationDate;

  private Date lastModifiedDate;

  protected Published() {

  }

  public Published(Long id, String title,
    String route, Date publicationDate, Date lastModifiedDate) {
    this.id = id;
    this.title = title;
    this.route = route;
    this.publicationDate = publicationDate;
    this.lastModifiedDate = lastModifiedDate;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getRoute() {
    return route;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public Date getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(Date publicationDate) {
    this.publicationDate = publicationDate;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Date lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

}