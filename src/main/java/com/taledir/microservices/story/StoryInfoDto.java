package com.taledir.microservices.story;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;

public class StoryInfoDto {

  @NotEmpty
	private String title;

  @NotEmpty
	private String route;

	private String subtitle;

	@PastOrPresent
	private Date publicationDate;

	@PastOrPresent
	private Date lastModifiedDate;

	public StoryInfoDto(String title, String route, String subtitle, Date publicationDate,
			Date lastModifiedDate) {
		this.title = title;
		this.route = route;
		this.subtitle = subtitle;
		this.publicationDate = publicationDate;
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
}
