package com.taledir.microservices.story;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class StoryBadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StoryBadRequestException(String arg0) {
		super(arg0);
	}

}

