package com.taledir.microservices.story;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StoryRepository extends JpaRepository<Story, Long>{

    Story findOneByRoute(String route);

    // @Query(value = "select s.title, sb.body " +
    // "from story s, story_body sb " +
    // "where s.id = sb.id " +
    // "and s.route = ?", nativeQuery = true)
    @Query("select new " +
    "   com.taledir.microservices.story.StoryTitleBodyDto(" +
    "       s.title, sb.body" +
    "   ) " +
    "from Story s " +
    "join s.storyBody sb " +
    "where s.route = :route")
    StoryTitleBodyDto findStoryTitleBodyByRoute(String route);

    @Query("select new java.lang.String(sb.body) " +
    "from Story s " +
    "join s.storyBody sb " +
    "where s.route = :route")
    String findStoryBodyByRoute(String route);

    Boolean existsByRoute(String route);

    @Query("select new " +
    "   com.taledir.microservices.story.AuthorStoryListItemDto(" +
    "   s.title, s.route, s.snippet, s.publicationDate, s.lastModifiedDate" +    
    "   ) " +
    "from StoryProfile sp " +
    "join sp.authorStories s " +
    "where sp.username = :username")
    Set<AuthorStoryListItemDto> findStoriesByUsername(String username);

    @Query("select new java.lang.String(sa.username) " +
    "from Story s " +
    "join s.storyAuthors sa " +
    "where s.id = :id")
    Set<String> findAuthorsByStory(Long id);

    // @Query(value = "select ss.title, ss.route, ss.snippet, ss.authorsString, o.username as owner " +
    // "from ( " + 
    // "   select s.owner_id as owner_id, s.title as title, s.route as route, s.snippet as snippet, string_agg(a.username, ',') as authorsString " + 
    // "   from story s, story_author sa, story_profile a " +
    // "   where s.id = sa.story_id " + 
    // "   and a.id = sa.author_id " + 
    // "   group BY s.id" +
    // " ) ss, story_profile o " +
    // "where o.id = ss.owner_id", nativeQuery = true) 
    @Query("select new " +
    "   com.taledir.microservices.story.StoryCardDto(" +
    "       s.title, s.route, s.snippet, array_agg(sa.username) as authors" +
    "   ) " +
    "from Story s " +
    "join s.storyAuthors sa " + 
    "group BY s.id")
    List<StoryCardDto> findStoryCards(Pageable currentPage);
}